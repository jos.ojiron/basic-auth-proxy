const http = require('http');
const httpProxy = require('http-proxy');
var auth = require('basic-auth')
var compare = require('tsscmp')

const port = 3100;
const username = process.env.USER_AUTH || "john";
const password = process.env.PASSWORD_AUTH || "Clinton1994!";

const proxy = httpProxy.createProxyServer({});

const server = http.createServer((req, res) => {
  var credentials = auth(req)

  // Check credentials
  // The "check" function will typically be against your user store
  if (!credentials || !check(credentials.name, credentials.pass)) {
    res.statusCode = 401
    res.setHeader('WWW-Authenticate', 'Basic realm="example"')
    res.end('Access denied')
    console.log('Access denied')
  } else {
    console.log('[%s] [forward] %s', new Date, req.url);
    proxy.web(req, res, {
      target: "http://localhost"
    });
  }
});

function check(name, pass) {
  var valid = true

  // Simple method to prevent short-circut and use timing-safe compare
  valid = compare(name, username) && valid
  valid = compare(pass, password) && valid

  return valid
}

server.timeout = 600000;

server.on('clientError', (err, socket) => {
  console.error('[%s] [error %s] %s', new Date, err.message);
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});

console.log("listening on port %d", port)
server.listen(port);